project "spirv-cross-reflect"
	language "C++"
	kind "StaticLib"

	systemversion "latest"

	files
	{
		"./spirv_reflect.cpp",
		"./spirv_reflect.hpp"
	}

	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"