project "spirv-cross-core"
	language "C++"
	kind "StaticLib"

	systemversion "latest"

	files
	{
		"./GLSL.std.450.h",
		"./spirv_common.hpp",
		"./spirv.hpp",
		"./spirv_cross.hpp",
		"./spirv_cross.cpp",
		"./spirv_parser.hpp",
		"./spirv_parser.cpp",
		"./spirv_cross_parsed_ir.hpp",
		"./spirv_cross_parsed_ir.cpp",
		"./spirv_cfg.hpp",
		"./spirv_cfg.cpp"
	}

	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"