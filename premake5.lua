workspace "spirv-cross"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	defaultplatform "x64"

	include "spirv-cross-core"
	include "spirv-cross-glsl"
	include "spirv-cross-hlsl"
	include "spirv-cross-reflect"